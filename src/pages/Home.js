//Styling and Animation
import styled from "styled-components";
import { motion, AnimatePresence, AnimateSharedLayout } from "framer-motion";
//redux
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadGames } from './../actions/gamesAction';
//Components
import { Game } from "../components/Game";
import { GameDetail } from "../components/GameDetail";
import { useLocation } from "react-router-dom";
import loadDetail from "../actions/detailAction";
import { fadeIn } from "../animation";

export const Home = () => {
  //get current location 
  const location = useLocation();
  const pathId = location.pathname.split('/')[2];
  //Fetch games
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadGames()).then(() => {
      if (pathId) {
        dispatch(loadDetail(pathId));
      }
    });
  }, [ dispatch ]);
  //get that data back
  const {popular, newGames, upcoming, searched } = useSelector(state => state.games);

  return (
    <GameList variants={fadeIn} initial="hidden" animate="show">
      <AnimateSharedLayout type="crossfade">
      <AnimatePresence>
        { pathId && <GameDetail pathId={pathId} /> }
      </AnimatePresence>
      {searched.length ? (
      <div className="searched">
        <h2>Searched Games</h2>
        <Games>
          {searched.map(game => (
            <Game
              key={game.id}
              name={game.name}
              relased={game.released}
              id={game.id}
              image={game.background_image} 
            />
          ))}
        </Games>
      </div>
      ): null}
      <h2>Upcoming Games</h2>
      <Games>
        {upcoming.map(game => (
          <Game
            key={game.id}
            name={game.name}
            relased={game.released}
            id={game.id}
            image={game.background_image} 
          />
        ))}
      </Games>
      <h2>Popular Games</h2>
      <Games>
        {popular.map(game => (
          <Game
            key={game.id}
            name={game.name}
            relased={game.released}
            id={game.id}
            image={game.background_image} 
          />
        ))}
      </Games>
      <h2>New Games</h2>
      <Games>
        {newGames.map(game => (
          <Game
            key={game.id}
            name={game.name}
            relased={game.released}
            id={game.id}
            image={game.background_image} 
          />
        ))}
      </Games>
      </AnimateSharedLayout>
    </GameList>
  );
};

const GameList = styled(motion.div)`
  padding: 0rem 5rem;
  h2 {
    padding: 5rem 0rem;
  }

  @media (max-width: 500px) {
    padding: 0rem 1rem;
    h2 {
      font-size: 2rem;
      padding: 1.2rem 0rem;
    }
  }
`;

const Games = styled(motion.div)`
  min-height: 80vh;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-column-gap: 3rem;
  grid-row-gap: 5rem;
`;