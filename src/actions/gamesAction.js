import axios from 'axios';
import { popularGamesURL, upcomingGamesUTL, newGameURL, searchGameUrl } from './../api';

//Action creator

export const loadGames = () => async (dispatch) =>{
  //Fetch axios
  const popularData = await axios.get(popularGamesURL());
  const newGamesData = await axios.get(newGameURL());
  const upcomingData = await axios.get(upcomingGamesUTL());

  dispatch({
    type: "FETCH_GAMES",
    payload: {
      popular: popularData.data.results,
      upcoming: upcomingData.data.results,
      newGames: newGamesData.data.results
    }
  });
};

export const fetchSearched = (game_name) => async (dispatch) => {
  //Fetch axios
  const searchGames = await axios.get(searchGameUrl(game_name));

  dispatch({
    type: 'FETCH_SEARCHED',
    payload: {
      searched: searchGames.data.results
    }
  });
};