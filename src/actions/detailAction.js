import axios from "axios";
import { gameDetailsURL, gameScreenURL } from "../api";

const loadDetail = (id) => async (dispatch) => {

  dispatch({
    type: 'LOADING_DETAIL'
  });

  const detailData = await axios.get(gameDetailsURL(id));
  const screenData = await axios.get(gameScreenURL(id));

  dispatch({
    type: 'GET_DETAIL',
    payload: {
      game: detailData.data,
      screen: screenData.data
    }
  })
};

export default loadDetail;